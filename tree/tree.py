import numpy as np
from sklearn.base import BaseEstimator


def entropy(y):
    EPS = 0.0005

    if y.size == 0:
        return 0.0

    return -np.sum((np.mean(y, axis=0) * np.log2(np.mean(y, axis=0) + EPS)))


def gini(y):
    return 1 - np.sum((np.mean(y, axis=0) ** 2))


def variance(y):
    if y.size == 0:
        return 0.0

    return np.var(y)


def mad_median(y):
    if y.size == 0:
        return 0.0

    return np.mean(np.abs(y - np.mean(y)))


def one_hot_encode(n_classes, y):
    y_one_hot = np.zeros((len(y), n_classes), dtype=float)
    y_one_hot[np.arange(len(y)), y.astype(int)[:, 0]] = 1.
    return y_one_hot


def one_hot_decode(y_one_hot):
    return y_one_hot.argmax(axis=1)[:, None]


class Node:

    def __init__(self, feature_index, threshold, proba=0):
        self.feature_index = feature_index
        self.value = threshold
        self.proba = proba
        self.left_child = None
        self.right_child = None


class DecisionTree(BaseEstimator):
    all_criterions = {
        'gini': (gini, True),
        'entropy': (entropy, True),
        'variance': (variance, False),
        'mad_median': (mad_median, False)
    }

    def __init__(self, n_classes=None, max_depth=np.inf, min_samples_split=2,
                 criterion_name='gini', debug=False):

        assert criterion_name in self.all_criterions.keys(), \
            'Criterion name must be on of the following: {}'.format(
                                            self.all_criterions.keys())

        self.n_classes = n_classes
        self.max_depth = max_depth
        self.min_samples_split = min_samples_split
        self.criterion_name = criterion_name

        self.depth = 0
        self.root = None
        self.debug = debug

    def make_split(self, feature_index, threshold, X_subset, y_subset):
        left_inds = X_subset[:, feature_index] < threshold

        X_left = X_subset[left_inds]
        y_left = y_subset[left_inds]

        X_right = X_subset[~left_inds]
        y_right = y_subset[~left_inds]

        return (X_left, y_left), (X_right, y_right)

    def make_split_only_y(self, feature_index, threshold, X_subset, y_subset):
        left_inds = X_subset[:, feature_index] < threshold

        y_left = y_subset[left_inds]
        y_right = y_subset[~left_inds]

        return y_left, y_right

    def choose_best_split(self, X_subset, y_subset):
        feature_index = 0
        threshold = .0
        best_score = np.inf

        for i in range(X_subset.shape[1]):
            features = np.unique(X_subset[:, i])
            for j in features:
                y_left, y_right = self.make_split_only_y(i,
                                                         j,
                                                         X_subset,
                                                         y_subset)
                left = (y_left.shape[0] / X_subset.shape[0]) * \
                    self.criterion(y_left)
                right = (y_right.shape[0] / X_subset.shape[0]) * \
                    self.criterion(y_right)
                G_score = left + right
                if G_score < best_score:
                    feature_index = i
                    threshold = j
                    best_score = G_score

        return feature_index, threshold

    def make_tree(self, X_subset, y_subset, depth=0):
        if X_subset.shape[0] <= self.min_samples_split or \
           depth >= self.max_depth:
            return np.mean(y_subset, axis=0)
        else:
            best_i, best_threshold = self.choose_best_split(X_subset, y_subset)
            (X_left, y_left), (X_right, y_right) = \
                self.make_split(best_i,
                                best_threshold,
                                X_subset,
                                y_subset)

            l_vertex = self.make_tree(X_left, y_left, depth + 1)
            r_vertex = self.make_tree(X_right, y_right, depth + 1)

            new_node = Node(best_i, best_threshold)
            new_node.left_child = l_vertex
            new_node.right_child = r_vertex

            return new_node

    def fit(self, X, y):
        assert len(y.shape) == 2 and len(y) == len(X), 'Wrong y shape'
        self.criterion, self.classification = \
            self.all_criterions[self.criterion_name]
        if self.classification:
            if self.n_classes is None:
                self.n_classes = len(np.unique(y))
            y = one_hot_encode(self.n_classes, y)

        self.root = self.make_tree(X, y)

    def predict(self, X):
        y_predicted = np.zeros(X.shape[0])

        for i in range(X.shape[0]):
            current_node = self.root

            while isinstance(current_node, Node):
                if X[i, current_node.feature_index] < current_node.value:
                    current_node = current_node.left_child
                else:
                    current_node = current_node.right_child

            if self.classification:
                y_predicted[i] = np.argmax(current_node)
            else:
                y_predicted[i] = current_node

        return y_predicted

    def predict_proba(self, X):
        assert self.classification, 'Available only for classification problem'

        y_predicted_probs = np.zeros((X.shape[0], self.n_classes))

        for i in range(X.shape[0]):
            current_node = self.root

            while isinstance(current_node, Node):
                if X[i, current_node.feature_index] < current_node.value:
                    current_node = current_node.left_child
                else:
                    current_node = current_node.right_child

            y_predicted_probs[i] = current_node

        return y_predicted_probs

coverage==4.5.1
flake8==3.6.0
pytest==3.10.1
pytest-cov==2.6.0
numpy==1.18.4
scikit-learn==0.22

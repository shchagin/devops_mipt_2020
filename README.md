### GitLab CI example 

In order to build this application on macOS Catalina (or higher) you will need the following software pre-installed: 

| **Requirement**                        | **Instruction**                                         |
| ---------------------------------------| ------------------------------------------------------- |
| `Python >= 3.8.3`                      | `https://www.python.org/downloads/`                   |
| `pip`                                  |  Download `https://bootstrap.pypa.io/get-pip.py` |
|               | Run it with python |
| |```python get-pip.py ``` |


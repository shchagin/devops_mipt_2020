import pytest

import numpy as np
from sklearn.base import BaseEstimator
from sklearn.datasets import load_digits
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

RANDOM_STATE = 42

from tree import entropy, gini, variance, mad_median, DecisionTree


def test_split():
	X = np.ones((4, 5), dtype=float) * np.arange(4)[:, None]
	y = np.arange(4)[:, None] + np.asarray([0.2, -0.3, 0.1, 0.4])[:, None]
	class_estimator = DecisionTree(max_depth=10, criterion_name='gini')

	(X_l, y_l), (X_r, y_r) = class_estimator.make_split(1, 1., X, y)

	assert np.array_equal(X[:1], X_l)
	assert np.array_equal(X[1:], X_r)
	assert np.array_equal(y[:1], y_l)
	assert np.array_equal(y[1:], y_r)


def test_shape():
	digits_data = load_digits().data
	digits_target = load_digits().target[:, None]
	X_train, X_test, y_train, y_test = train_test_split(digits_data, 
														digits_target, 
														test_size=0.2, 
														random_state=RANDOM_STATE)
	assert len(y_train.shape) == 2 and y_train.shape[0] == len(X_train)


def test_gini():
	digits_data = load_digits().data
	digits_target = load_digits().target[:, None]
	X_train, X_test, y_train, y_test = train_test_split(digits_data, 
														digits_target, 
														test_size=0.2, 
														random_state=RANDOM_STATE)
	
	class_estimator = DecisionTree(max_depth=10, criterion_name='gini')
	class_estimator.fit(X_train, y_train)
	ans = class_estimator.predict(X_test)
	accuracy_gini = accuracy_score(y_test, ans)

	assert  0.84 < accuracy_gini < 0.9


def test_entropy():
	digits_data = load_digits().data
	digits_target = load_digits().target[:, None]
	X_train, X_test, y_train, y_test = train_test_split(digits_data, 
														digits_target, 
														test_size=0.2, 
														random_state=RANDOM_STATE)
	
	class_estimator = DecisionTree(max_depth=10, criterion_name='entropy')
	class_estimator.fit(X_train, y_train)
	ans = class_estimator.predict(X_test)
	accuracy_entropy = accuracy_score(y_test, ans)

	assert  0.86 < accuracy_entropy < 0.9

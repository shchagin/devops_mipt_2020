from setuptools import setup, find_packages

ROOT_PACKAGE_NAME = 'tree'


def parse_requirements():
    with open('requirements.txt') as f:
        return f.read().splitlines()


setup(
    name=ROOT_PACKAGE_NAME,
    version='1.0',
    author=['Evgenii Shchagin inspired by Pavel Akhtyamov'],
    packages=find_packages(),
    long_description='tree',
    requirements=parse_requirements()
)
